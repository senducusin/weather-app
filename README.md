**Weather App using MVVM**

This is a sample app that utilizes calling of GET HTTP requests, UIKit, and Master-Detail view. The source code, demonstrates a simple implementation of MVVM structure in Swift.
  
---

## API

The app uses an API provided by https://openweathermap.org/

---

## Features

- Let's you input a city name and display that city's temperature in a list format
- The temperature can be shown in Fahrenheit or Celsius. Temperature unit can be set on the Settings
- Selecting a city from the list will take you to a detail view, which contains the city name, current temperature, maximum temperature, and minimum temperature