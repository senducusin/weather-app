//
//  WeatherListViewModelTests.swift
//  WeatherAppTests
//
//  Created by Jansen Ducusin on 2/15/21.
//

import XCTest
@testable import WeatherApp

class WeatherListViewModelTests: XCTestCase {

    private var weatherListViewModel: WeatherListViewModel!
    
    override func setUp() {
        self.weatherListViewModel = WeatherListViewModel()
        
        self.weatherListViewModel.addCityWeather(Weather(name: "Candon", currentTemperature: Temperature(temperature: 0, temperatureMin: 0, temperatureMax: 0)))
        
        self.weatherListViewModel.addCityWeather(Weather(name: "Baguio", currentTemperature: Temperature(temperature: 22.22222, temperatureMin: 0, temperatureMax: 0)))
    }
    
    func test_shouldBeAbleToConvertToCelsiusSuccessfully(){
        let celsiusTemperature = [32, 72]
        
        self.weatherListViewModel.updateUnit(to: .fahrenheit)
        
        for n in 0..<self.weatherListViewModel.totalNumberOfCityWeathers(){
            let cityWeather = self.weatherListViewModel.cityWeatherAt(n)
            
            XCTAssertEqual(Int(round(cityWeather.currentTemperature.temperature)), celsiusTemperature[n])
        }
    }
    
    override class func tearDown() {
        super.tearDown()
    }

}
