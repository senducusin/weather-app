//
//  SettingsViewModelTests.swift
//  WeatherAppTests
//
//  Created by Jansen Ducusin on 2/15/21.
//

import XCTest
@testable import WeatherApp

class SettingsViewModelTests: XCTestCase {

    private var settingsViewModel: SettingsViewModel!
    
    override func setUp() {
        super.setUp()
        
        self.settingsViewModel = SettingsViewModel()
    }
    
    func test_shouldReturnCorrectDisplayNameForCelsius(){
        XCTAssertEqual(self.settingsViewModel.selectedUnit.displayName, "Celsius")
    }
    
    func test_shouldMakeSureThatDefaultSelectedUnitIsCelsius(){
        XCTAssertEqual(settingsViewModel.selectedUnit, .celsius)
    }
    
    func test_shouldBeAbleToSaveUserUnitSelection(){
        self.settingsViewModel.selectedUnit = .fahrenheit
        let userDefaults = UserDefaults.standard
        
        XCTAssertNotNil(userDefaults.value(forKey: "unit"))
    }
    
    override class func tearDown() {
        super.tearDown()
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "unit")
    }

}
