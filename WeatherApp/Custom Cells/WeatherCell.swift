//
//  WeatherCell.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/10/21.
//

import Foundation
import UIKit

class WeatherCell:  UITableViewCell{
    
    @IBOutlet weak var cityNameLabel:   UILabel!
    @IBOutlet weak var temperatureLabel:    UILabel!
    
    func configure(_ weatherCity: Weather){
        self.cityNameLabel.text = weatherCity.name
        self.temperatureLabel.text = weatherCity.currentTemperature.temperature.formatAsDegree
    }
       
}
