//
//  WeatherDetailViewModel.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/13/21.
//

import Foundation

struct DetailProperty{
    let propertyName:   String
    let propertyValue:  String
}

struct WeatherDetailViewModel {
    private var details = [DetailProperty]()
    
    var cityWeather: Weather?
    
    func numberOfRows(_ section: Int) -> Int {
        return details.count
    }
    
    func detailAt(_ index: Int) -> DetailProperty {
        return self.details[index]
    }
}

extension WeatherDetailViewModel{
    mutating func initialize(cityWeather:Weather){
        self.cityWeather = cityWeather
        getPropertyAndValue()
    }
    
    private func getUnit() -> String{
        let userDefaults = UserDefaults.standard
        
        if let value = userDefaults.value(forKey: "unit") as? String {
            return String(value.prefix(1)).capitalized
        }
        
        return "C"
    }
    
    private mutating func getPropertyAndValue(){
        if let cityWeather = self.cityWeather {
            let cityWeatherMirror = Mirror(reflecting: cityWeather)
            
            for(label, value) in cityWeatherMirror.children {
                guard let label = label else { continue }
                
                if let value = value as? String {
                    
                    details.append(DetailProperty(propertyName: label.capitalized, propertyValue: value))
                    
                }else if let value = value as? Temperature {
                    let temperateureMirror = Mirror(reflecting: value)
                    
                    for (label, value) in temperateureMirror.children {
                        
                        guard let label = label else {continue}
                        
                        if let value = value as? Double {
                            details.append(DetailProperty(propertyName: label.camelCaseToWords().capitalized, propertyValue: "\(value.formatAsDegree) \(getUnit())"))
                        }
                    }
                }
            }
        }
    }
    
}
