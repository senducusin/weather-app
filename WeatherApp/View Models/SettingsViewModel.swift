//
//  SettingsViewModel.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/11/21.
//

import Foundation

enum Unit:  String, CaseIterable{
    case celsius
    case fahrenheit
}

extension Unit{
    var displayName:    String{
        get {
            switch (self) {
            case .celsius:
                return "Celsius"
            case .fahrenheit:
                return "Fahrenheit"
            }
        }
    }
}

struct SettingsViewModel {
    let units = Unit.allCases
    private var _selectedUnit:   Unit = Unit.celsius
    
    var selectedUnit:   Unit{
        get{
            let userDefaults = UserDefaults.standard
            
            if let value = userDefaults.value(forKey: "unit") as? String {
                return Unit(rawValue: value)!
            }
            
            return _selectedUnit
        } set {
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue.rawValue, forKey: "unit")
        }
    }
    
    func numberOfRows(_ section:    Int) -> Int{
        return self.units.count
    }
    
    func unitAt(_ index:    Int) -> Unit {
        return self.units[index]
    }
}
