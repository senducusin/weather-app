//
//  WeatherListViewModel.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/11/21.
//

import Foundation

struct WeatherListViewModel{
    
    private var cityWeathers = [Weather]()
    
    mutating func addCityWeather(_ cityWeather: Weather){
        
        let currentCities = self.cityWeathers.map { cityWeather in
            return cityWeather.name
        }
        
        if currentCities.contains(cityWeather.name) == false{
            self.cityWeathers.append(cityWeather)
        }
    }
    
    func totalNumberOfCityWeathers() -> Int {
        return cityWeathers.count
    }
    
    func numberOfRows(_ section: Int) -> Int{
        return self.cityWeathers.count
    }
    
    func cityWeatherAt(_ index: Int) -> Weather{
        return self.cityWeathers[index]
    }
    
    private func computeToCelsius(temp:Double) -> Double {
        return (temp - 32) * 5/9
    }
    
    private func computeToFahrenheit(temp:Double) -> Double {
        return (temp * 9/5) + 32
    }
    
    mutating func toCelsius() {
        cityWeathers = cityWeathers.map { cityWeather in
            var weather = cityWeather
            weather.currentTemperature.temperature = computeToCelsius(temp: weather.currentTemperature.temperature)
            weather.currentTemperature.temperatureMax = computeToCelsius(temp: weather.currentTemperature.temperatureMax)
            weather.currentTemperature.temperatureMin = computeToCelsius(temp: weather.currentTemperature.temperatureMin)
            return weather
        }
    }
    
    mutating func toFahrenheit() {
        cityWeathers = cityWeathers.map { cityWeather in
            var weather = cityWeather
            weather.currentTemperature.temperature = computeToFahrenheit(temp: weather.currentTemperature.temperature)
            weather.currentTemperature.temperatureMax = computeToFahrenheit(temp: weather.currentTemperature.temperatureMax)
            weather.currentTemperature.temperatureMin = computeToFahrenheit(temp: weather.currentTemperature.temperatureMin)
            return weather
        }
    }
    
    mutating func updateUnit(to unit: Unit){
        switch unit {
            case .celsius:
                toCelsius()
            case .fahrenheit:
                toFahrenheit()
        }
    }
}
