//
//  WebService.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/11/21.
//

import Foundation

struct Account{
    var apiKey = "1c3d36af16ecaa4d2191a167d5b652d5"
}

struct Resource<T> {
    let url:    URL
    let parse:  (Data) -> T?
}

class WebService{
    func load<T>(resource:  Resource<T>, completion:@escaping (T?) -> ()){
        
        URLSession.shared.dataTask(with: resource.url) { data, response, error in
            
            if let data = data {
                DispatchQueue.main.async {
                    completion(resource.parse(data))
                }
                
            }else{
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }.resume()

    }
}
