//
//  Double+Extensions.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/11/21.
//

import Foundation

extension Double {
    var formatAsDegree:String{
        return String(format:"%.0f°",self)
    }
}
