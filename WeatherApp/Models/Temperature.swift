//
//  Temperature.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/11/21.
//

import Foundation

struct Temperature: Decodable {
    var temperature:    Double
    var temperatureMin: Double
    var temperatureMax: Double
    
    private enum CodingKeys:    String, CodingKey{
        case temperature = "temp"
        case temperatureMin = "temp_min"
        case temperatureMax = "temp_max"
    }
}
