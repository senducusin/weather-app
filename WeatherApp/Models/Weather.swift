//
//  Weather.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/11/21.
//

import Foundation

struct Weather: Decodable {
    let name:   String
    var currentTemperature:   Temperature
    
    private enum CodingKeys:    String, CodingKey{
        case name
        case currentTemperature = "main"
    }
}
