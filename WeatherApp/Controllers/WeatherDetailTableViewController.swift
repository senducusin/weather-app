//
//  WeatherDetailTableViewController.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/13/21.
//

import Foundation
import UIKit

class WeatherDetailTableViewController: UITableViewController{
    var cityWeather: Weather?
    var weatherDetail = WeatherDetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let cityWeather = self.cityWeather {
            self.weatherDetail.initialize(cityWeather: cityWeather)
            self.tableView.reloadData()
        }
    }
 
}

extension WeatherDetailTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherDetail.numberOfRows(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherDetailCell")!
        let detail = weatherDetail.detailAt(indexPath.row)
        
        cell.textLabel?.text = detail.propertyName
        cell.detailTextLabel?.text = detail.propertyValue
        return cell
    }
}
