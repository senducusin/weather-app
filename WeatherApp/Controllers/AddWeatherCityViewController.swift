//
//  AddWeatherCityViewController.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/10/21.
//

import Foundation
import UIKit

protocol AddWeatherDelegate {
    func addWeatherDidSave(cityWeather: Weather)
}

class AddWeatherCityViewController:UIViewController{
    
    @IBOutlet weak var cityNameTextField:   UITextField!
    
    var delegate: AddWeatherDelegate?
    
    @IBAction func saveCityButtonPressed(){
        
        if let city = cityNameTextField.text {
            
            let urlString = getApiUrl(city: city)
            let weatherURL = URL(string: urlString)!
            
            let weatherResource = Resource<Weather>(url: weatherURL){ data in
                
                let weather = try? JSONDecoder().decode(Weather.self, from: data)
                return weather
            }
            
            WebService().load(resource: weatherResource){ [weak self] result in
                if let weather = result {
                    if let delegate = self?.delegate{
                        delegate.addWeatherDidSave(cityWeather: weather)
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            }
            
        }
        
    }
    
    @IBAction func cancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func getApiUrl(city:String) -> String{
        let account = Account()
        let userDefaults = UserDefaults.standard
        var unit = "metric"
        
        if let value = userDefaults.value(forKey: "unit") as? String {
            unit = value == "fahrenheit" ? "imperial" : "metric"
        }
        
        
        return "https://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=\(account.apiKey)&units=\(unit)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
    
}
