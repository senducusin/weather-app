//
//  WeatherListTableViewController.swift
//  WeatherApp
//
//  Created by Jansen Ducusin on 2/10/21.
//

import Foundation
import UIKit

class WeatherListTableViewController:   UITableViewController{
    
    var cityWeathers = WeatherListViewModel()
    private var currentUnit:String = Unit.celsius.rawValue
    private var updateValues:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let userDefaults = UserDefaults.standard
        if let loadedUnit = userDefaults.value(forKey: "unit") as? String {
            currentUnit = loadedUnit
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AddCityWeatherSegue"{
            
            prepareSegueForAddCityWeather(segue: segue)
            
        }else if segue.identifier == "SettingsSegue"{
            
            prepareSegueForSettings(segue: segue)
            
        } else if segue.identifier == "WeatherCityDetailSegue"{
            
            prepareSegueForWeatherDetails(segue: segue)
            
        }
    }
    
    private func prepareSegueForWeatherDetails(segue:UIStoryboardSegue){
        guard let weatherDetailsVC = segue.destination as? WeatherDetailTableViewController,
              let indexPath = self.tableView.indexPathForSelectedRow else {
            return
        }
        
        let cityWeather = self.cityWeathers.cityWeatherAt(indexPath.row)
        weatherDetailsVC.cityWeather = cityWeather
    }
    
    private func prepareSegueForAddCityWeather(segue:UIStoryboardSegue){
        guard let nav = segue.destination as? UINavigationController else {
            fatalError("Navigation Controller is missing!")
        }
        
        guard let addWeatherCityVC = nav.viewControllers.first as? AddWeatherCityViewController else {
            fatalError("AddWeatherCityVC is missing!")
        }
        
        addWeatherCityVC.delegate = self
    }
    
    private func prepareSegueForSettings(segue:UIStoryboardSegue){
        guard let nav = segue.destination as? UINavigationController else {
            fatalError("Navigation Controller is missing!")
        }
        
        guard let settingsTableVC = nav.viewControllers.first as? SettingsTableViewController else {
            fatalError("Settings Table VC is missing!")
        }
        
        settingsTableVC.delegate = self
    }
}

extension WeatherListTableViewController: SettingsDelegate{
    func settingsDidTapDone(settings: SettingsViewModel) {
        
        if settings.selectedUnit.rawValue != self.currentUnit {
            self.currentUnit = settings.selectedUnit.rawValue
            self.cityWeathers.updateUnit(to: settings.selectedUnit)
            tableView.reloadData()
        }
    }
}

extension WeatherListTableViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityWeathers.numberOfRows(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        
        let cityWeather = self.cityWeathers.cityWeatherAt(indexPath.row)
        cell.configure(cityWeather)
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension WeatherListTableViewController: AddWeatherDelegate{
    func addWeatherDidSave(cityWeather: Weather) {
        self.cityWeathers.addCityWeather(cityWeather)
        tableView.reloadData()
    }
}
